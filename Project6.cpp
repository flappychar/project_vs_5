﻿#include <iostream>
#include <math.h>

using namespace std;

class Vector
{
private:
    double x, y, z;

public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    double LenghVector()
    {
        return sqrt(pow(x,2) + pow(y, 2) + pow(z, 2));
    }

    void ShowVector()
    {
        cout << x << ' ' << y << ' ' << z << "\n";
        cout << LenghVector();
    }
};

int main()
{
    Vector vector(25.3, 10.25, 15.5);
    
    vector.ShowVector();

}